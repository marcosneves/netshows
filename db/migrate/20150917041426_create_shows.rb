class CreateShows < ActiveRecord::Migration
  def change
    create_table :shows do |t|
      t.references :band, index: true, foreign_key: true
      t.string :address
      t.datetime :start_at
      t.text :description

      t.timestamps null: false
    end
  end
end
