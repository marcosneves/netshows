DatabaseCleaner.clean_with(:truncation)

categories = FactoryGirl.create_list(:category, 5)

bands = FactoryGirl.build_list(:band, 20, category: nil) do |obj|
  obj.category = categories.sample
  obj.save
end

FactoryGirl.build_list(:show, 40, band: nil) do |obj|
  obj.band = bands.sample
  obj.save
end

AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')
