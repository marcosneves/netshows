class ShowDecorator < Draper::Decorator
  delegate_all
  delegate :name, to: :band, prefix: true
  delegate :category, to: :band
  delegate :year, to: :start_at

  def day
   '%02d' % start_at.day
  end

  def seconds_until_start
    start_at.to_i - Time.current.to_i
  end
  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

end
