class Category < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true
  has_many :bands

  def to_s
   name
  end
end

# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
