class Show < ActiveRecord::Base
  belongs_to :band
  validates :band, :address, :start_at, :description, presence: true

  scope :upcoming, -> {
    includes(band: :category)
    .where('start_at >= ?', Time.current)
    .order(:start_at)
  }
end

# == Schema Information
# Schema version: 20150920120657
#
# Table name: shows
#
#  id          :integer          not null, primary key
#  band_id     :integer
#  address     :string
#  start_at    :datetime
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  image_url   :string
#
# Indexes
#
#  index_shows_on_band_id  (band_id)
#
