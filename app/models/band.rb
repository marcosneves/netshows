class Band < ActiveRecord::Base
  belongs_to :category
  validates :name, presence: true
  validates :category, presence: true

  scope :index, -> { includes(:category).order(:name) }

  def to_s
    name
  end
end

# == Schema Information
#
# Table name: bands
#
#  id          :integer          not null, primary key
#  name        :string
#  category_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_bands_on_category_id  (category_id)
#
