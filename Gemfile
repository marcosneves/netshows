source 'https://rubygems.org'
ruby '2.2.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.4'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Use sqlite3 as the database for Active Record
  gem 'sqlite3'

  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  gem 'pry-rails', '~> 0.3.4'

  # precisa estar no dev para funcionar os generators, ou utilizar "RAILS_ENV=test rails generate ..."
  gem 'rspec-rails', '~> 3.3.3'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-commands-rspec', '~> 1.0.4'
  gem 'rack-livereload', '~> 0.3.16'
  gem 'bullet', '~> 4.14.7'
  gem 'annotate', '~> 2.6.10'
end

group :test do
  gem 'shoulda-matchers', '~> 3.0.0.rc1'
  gem 'db-query-matchers', '~> 0.4.0'
end

group :console do
  gem 'guard', '~> 2.13'
  gem 'guard-livereload', '~> 2.4'
  gem 'guard-rails', '~> 0.7.2'
  gem 'guard-rspec', '~> 4.6.4'
  gem 'guard-bundler', '~> 2.1.0'

  gem 'simplecov', '~> 0.10'
  # gem 'coco', '~> 0.13.0'
  gem 'awesome_print', '~> 1.6.1'
end

group :production do
  gem 'rails_12factor'
  gem 'pg'
  gem 'puma'
end

source 'https://rails-assets.org' do
  gem 'rails-assets-vide', '~> 0.3.7'
  gem 'rails-assets-animate.css', '~> 3.4.0'
  gem 'rails-assets-wow', '~> 1.1.2'
  gem 'rails-assets-flipclock', '~> 0.7.7'
end

gem 'factory_girl_rails', '~> 4.5'
gem 'forgery', '~> 0.6'
gem 'database_cleaner', '~> 1.5'

gem 'lograge', '~> 0.3.4'
gem 'slim-rails', '~> 3.0.1'
gem 'draper', '~> 2.1.0'
gem 'activeadmin', github: 'activeadmin'
gem 'devise', '~> 3.5.2'
