# http://loremflickr.com/
# https://placeimg.com/
# http://xoart.link/
# http://unsplash.it/
# http://lorempixel.com/
# http://p-hold.com/
class Forgery::Image < Forgery
  def self.url(width = 800, height = 600, tag = nil, index = nil)
    tag   ||= rand(10000)
    index ||= rand(100)
    "http://xoart.link/#{width}/#{height}/#{tag}/#{index % 100}.jpg"
  end

  def self.tags
    %w(abstract architecture art books business capibara cars cats city cute design dogs) +
    %w(electronics elephant fashion food gardening hashtag history industry kids love music nature) +
    %w(nightlife outdoor people photography sea skyline sport stars starwars technics web zagreb)
  end
end
