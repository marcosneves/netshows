module InfinityRewindEnumerator
  def next
    super
  rescue StopIteration
    rewind
    super
  end
end

module RandomSequence
  def random_sequence
    @enum ||= shuffle.to_enum.extend InfinityRewindEnumerator
    @enum.next
  end
end

Forgery::Extensions::Array.send :include, RandomSequence

class Forgery::Music < Forgery
  def self.genre(index = nil)
    @@dict ||= Forgery::Extend(dictionaries[:music_genres].shuffle)
    if index
      @@dict[index % @@dict.length]
    else
      @@dict.random.unextend
    end
  end

  def self.genre_sequence
    dictionaries[:music_genres].random_sequence
  end

  def self.band
    dictionaries[:music_bands].random.unextend
  end
end
