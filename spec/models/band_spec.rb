# == Schema Information
#
# Table name: bands
#
#  id          :integer          not null, primary key
#  name        :string
#  category_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_bands_on_category_id  (category_id)
#

require 'rails_helper'

RSpec.describe Band, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:category) }
  it { should belong_to(:category) }
  it { should cast_to_string_using(:name) }

  describe '.index' do
    it 'should includes :category' do
      create(:band)
      expect { described_class.index.to_a }.to make_database_queries(count: 2)
    end
  end
end
