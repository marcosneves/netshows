# == Schema Information
# Schema version: 20150920120657
#
# Table name: shows
#
#  id          :integer          not null, primary key
#  band_id     :integer
#  address     :string
#  start_at    :datetime
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  image_url   :string
#
# Indexes
#
#  index_shows_on_band_id  (band_id)
#

require 'rails_helper'

RSpec.describe Show, type: :model do
  it { should validate_presence_of(:band) }
  it { should validate_presence_of(:address) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:start_at) }

  it { should belong_to(:band) }

  describe '.upcoming' do
    it 'should includes :band and :category' do
      create(:show, :upcoming)
      expect { described_class.upcoming.to_a }.to make_database_queries(count: 3)
    end
  end
end
