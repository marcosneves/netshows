FactoryGirl.define do
  factory :show do
    band
    address { Forgery(:address).city }
    start_at { Forgery(:date).date(max_delta: 365) + (0..23).to_a.sample.hours }
    description { Forgery(:lorem_ipsum).paragraphs(3, random: true) }
    sequence(:image_url) { |n| Forgery(:image).url(266, 150, 'music', n) }

    trait :upcoming do
      start_at { Time.current.tomorrow }
    end

  end
end

# == Schema Information
# Schema version: 20150920120657
#
# Table name: shows
#
#  id          :integer          not null, primary key
#  band_id     :integer
#  address     :string
#  start_at    :datetime
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  image_url   :string
#
# Indexes
#
#  index_shows_on_band_id  (band_id)
#
