FactoryGirl.define do
  factory :band do
    name { Forgery(:music).band }
    category
  end
end

# == Schema Information
# Schema version: 20150917041426
#
# Table name: bands
#
#  id          :integer          not null, primary key
#  name        :string
#  category_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_bands_on_category_id  (category_id)
#
