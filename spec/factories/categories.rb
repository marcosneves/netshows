FactoryGirl.define do
  factory :category do
    name { Forgery(:music).genre_sequence }
  end
end

# == Schema Information
# Schema version: 20150917041426
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
