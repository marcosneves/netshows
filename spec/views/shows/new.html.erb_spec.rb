require 'rails_helper'

RSpec.describe "shows/new", type: :view do
  before(:each) do
    assign(:show, Show.new(
      :band => nil,
      :address => "MyString",
      :description => "MyText"
    ))
  end

  it "renders new show form" do
    render

    assert_select "form[action=?][method=?]", shows_path, "post" do

      assert_select "input#show_band_id[name=?]", "show[band_id]"

      assert_select "input#show_address[name=?]", "show[address]"

      assert_select "textarea#show_description[name=?]", "show[description]"
    end
  end
end
