require 'rails_helper'

RSpec.describe "shows/edit", type: :view do
  before(:each) do
    @show = assign(:show, create(:show))
  end

  it "renders the edit show form" do
    render

    assert_select "form[action=?][method=?]", show_path(@show), "post" do

      assert_select "input#show_band_id[name=?]", "show[band_id]"

      assert_select "input#show_address[name=?]", "show[address]"

      assert_select "textarea#show_description[name=?]", "show[description]"
    end
  end
end
