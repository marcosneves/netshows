require 'rails_helper'

RSpec.describe "shows/show", type: :view do
  before(:each) do
    @show = assign(:show, create(:show).decorate)
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(@show.address)
    expect(rendered).to match(@show.description)
  end
end
