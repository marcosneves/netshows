require 'rails_helper'

RSpec.describe "categories/index", type: :view do
  before(:each) do
    assign :categories, create_list(:category, 2)
  end

  it "renders a list of categories" do
    render
    assert_select "tr", :count => 3
  end
end
