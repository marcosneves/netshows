require 'rails_helper'

RSpec.describe "bands/new", type: :view do
  before(:each) do
    assign(:band, Band.new(
      :name => "MyString",
      :category => nil
    ))
  end

  it "renders new band form" do
    render

    assert_select "form[action=?][method=?]", bands_path, "post" do

      assert_select "input#band_name[name=?]", "band[name]"

      assert_select "input#band_category_id[name=?]", "band[category_id]"
    end
  end
end
