require 'rails_helper'

RSpec.describe "bands/edit", type: :view do
  before(:each) do
    @band = assign(:band, Band.create!(
      :name => "MyString",
      :category => create(:category)
    ))
  end

  it "renders the edit band form" do
    render

    assert_select "form[action=?][method=?]", band_path(@band), "post" do

      assert_select "input#band_name[name=?]", "band[name]"

      assert_select "input#band_category_id[name=?]", "band[category_id]"
    end
  end
end
