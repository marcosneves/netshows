require 'rails_helper'

RSpec.describe "bands/index", type: :view do
  before(:each) do
    assign(:bands, [
      Band.create!(
        :name => "Name",
        :category => create(:category)
      ),
      Band.create!(
        :name => "Name",
        :category => create(:category)
      )
    ])
  end

  it "renders a list of bands" do
    render
    assert_select "tr", count: 3
  end
end
